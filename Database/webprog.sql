/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.16-MariaDB : Database - webprog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`webprog` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `webprog`;

/*Table structure for table `data` */

DROP TABLE IF EXISTS `data`;

CREATE TABLE `data` (
  `FirstName` varchar(99) DEFAULT NULL,
  `LastName` varchar(99) DEFAULT NULL,
  `Username` varchar(99) DEFAULT NULL,
  `Email` varchar(99) DEFAULT NULL,
  `Password` varchar(99) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `data` */

insert  into `data`(`FirstName`,`LastName`,`Username`,`Email`,`Password`) values ('rv','diaz','bogart','rossvincentdiaz12@gmail.com','d8578edf8458ce06fbc5bb76a58c5ca4');

/*Table structure for table `info` */

DROP TABLE IF EXISTS `info`;

CREATE TABLE `info` (
  `filename` varchar(99) DEFAULT NULL,
  `name` varchar(99) DEFAULT NULL,
  `size` varchar(99) DEFAULT NULL,
  `type` varchar(99) DEFAULT NULL,
  `category` varchar(99) DEFAULT NULL,
  `date` varchar(99) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `info` */

insert  into `info`(`filename`,`name`,`size`,`type`,`category`,`date`) values ('maiafire.png','bogart','8504','image/png','Pictures','02/25/18');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
